"""several methods for testing order and item class, for use of pytest, some unit tests"""
from order import NormItem, Order


# different unit tests
def order_test() -> None:
    order = Order("Pepe Frog", [])
    order.add_norm_item(NormItem("Pepe Bobble-head", 6, 6))
    order.add_norm_item(NormItem("Kekistani Sticker", 1, 23))
    order.add_norm_item(NormItem("Smignanoff figurine", 77, 1))
    assert order.total == 136


def norm_item_test() -> None:
    norm_item = NormItem("Pepe Bobble-head", 6, 6)
    assert norm_item.total == 36


def norm_item_remove_test() -> None:
    pepe = NormItem("Pepe Bobble-head", 6, 6)
    kek = NormItem("Kekistani Sticker", 1, 23)
    smi = NormItem("Smignanoff figurine", 77, 1)
    order = Order("Pepe Frog", [pepe, kek, smi])
    order.remove_norm_item(pepe)
    assert order.total == 100


def update_norm_quantity_test() -> None:
    pepe = NormItem("Pepe Bobble-head", 6, 6)
    kek = NormItem("Kekistani Sticker", 1, 23)
    smi = NormItem("Smignanoff figurine", 77, 1)
    order = Order("Pepe Frog", [pepe, kek, smi])
    order.update_norm_quantity(pepe, 2)
    assert order.total == 112
