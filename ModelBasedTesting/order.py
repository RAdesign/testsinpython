"""order class to test, simple item class, and order class for collecting items in order"""
from dataclasses import dataclass, field


@dataclass
class NormItem:
    description: str
    price: int
    quantity: int

    @property
    def total(self) -> int:
        return self.price * self.quantity


@dataclass
class Order:
    customer: str
    norm_items: list[NormItem] = field(default_factory=list)
    _total_cache: int = 0

    def __post_init__(self) -> None:  # updating total value of order post initialization
        self._update_total_cache()

    def add_norm_item(self, norm_item: NormItem) -> None:  # adding and updating
        self.norm_items.append(norm_item)
        self._update_total_cache()

    def remove_norm_item(self, norm_item: NormItem) -> None:  # removing and updating
        if norm_item not in self.norm_items:  # handle item not in order
            return
        self.norm_items.remove(norm_item)
        self._update_total_cache()

    def update_norm_quantity(self, norm_item: NormItem, quantity: int) -> None:
        if norm_item not in self.norm_items:  # handle item not in order
            return
        norm_item.quantity = quantity
        self._update_total_cache()

    def _update_total_cache(self) -> None:  # updating value of an order
        self._total_cache = sum(norm.total for norm in self.norm_items)

    @property  # accessible property with total value of an order
    def total(self) -> int:
        return self._total_cache
