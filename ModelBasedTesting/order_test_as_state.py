"""model tests class with rules and preconditions"""
import unittest
import hypothesis.strategies as strtg
import pytest
from hypothesis.stateful import RuleBasedStateMachine, precondition, rule
from order import NormItem, Order


class TestOrder(RuleBasedStateMachine):  # state machine the model based test is going to use
    def __init__(self) -> None:
        super().__init__()
        self.order = Order("Pepe Frog", [])  # order to be tested
        self.norm_items: list[NormItem] = []  # keep track of items

    # a rule to the first action of creating item
    @rule(
        description=strtg.text(),
        price=strtg.integers(),
        quantity=strtg.integers(),
    )
    # first action to be tested, creating and adding item
    def create_norm_item(self, description: str, price: int, quantity: int) -> None:
        self.norm_items.append(NormItem(description, price, quantity))

    # a rule, precondition and action to be tested - adding item to order
    @precondition(lambda self: len(self.norm_items) > 0)  # a precondition checking if items to add exist
    @rule(data=strtg.data())
    def add_norm_item_to_order(self, data: strtg.SearchStrategy) -> None:
        norm_item = data.draw(strtg.sampled_from(self.norm_items))
        self.order.add_norm_item(norm_item)

    # try to remove items if there are any in the order
    @precondition(lambda self: len(self.order.norm_items) > 0)
    @rule(data=strtg.data())
    def remove_norm_item_from_order(self, data: strtg.SearchStrategy) -> None:
        norm_item = data.draw(strtg.sampled_from(self.order.norm_items))  # remove from order
        self.order.remove_norm_item(norm_item)

    # try whether removing item from empty order would cause an exception
    @precondition(
        lambda self: len(self.order.norm_items) == 0 and len(self.norm_items) > 0
    )
    @rule(data=strtg.data())
    def remove_norm_item_from_empty_order_raise_exception(self, data: strtg.SearchStrategy) -> None:
        norm_item = data.draw(strtg.sampled_from(self.norm_items))
        with pytest.raises(ValueError):
            self.order.remove_norm_item(norm_item)

    # try whether removing item not in the order would cause an exception
    @precondition(
        lambda self: any(x not in self.order.norm_items for x in self.norm_items)
    )
    @rule(data=strtg.data())
    def remove_norm_item_from_empty_order_raise_exception(self, data: strtg.SearchStrategy) -> None:
        unordered_items = [x for x in self.norm_items if x not in self.order.norm_items]
        norm_item = data.draw(strtg.sampled_from(unordered_items))
        with pytest.raises(ValueError):
            self.order.remove_norm_item(norm_item)

    @precondition(lambda self: len(self.norm_items) > 0)
    @rule(data=strtg.data(), quantity=strtg.integers())
    def update_norm_item_quantity(self, data: strtg.SearchStrategy, quantity: int) -> None:
        norm_item = data.draw(strtg.sampled_from(self.norm_items))
        self.order.update_norm_quantity(norm_item, quantity)

    # validity check of total amount , with rule and definition
    @rule()
    def total_check(self) -> None:
        assert sum(norm.total for norm in self.order.norm_items) == self.order.total

    # special case
    @precondition(lambda self: len(self.order.norm_items) == 0)
    @rule()
    def total_check_zero(self) -> None:
        assert self.order.total == 0


# order test cas is separated from machine state, to use with pytest
OrderTestCase: unittest.TestCase = TestOrder.TestCase
