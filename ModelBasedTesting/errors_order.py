"""items and order class , not ideal classes, with caching errors"""
from dataclasses import dataclass, field


@dataclass
class NormItem:
    description: str
    price: int
    quantity: int

    @property
    def total(self) -> int:
        return self.price * self.quantity


@dataclass
class Order:
    customer: str
    norm_items: list[NormItem] = field(default_factory=list)
    _total_cache: int = 0

    def __post_init__(self) -> None:
        self._total_cache = sum(norm.total for norm in self.norm_items)

    def add_norm_item(self, norm_item: NormItem) -> None:
        self.norm_items.append(norm_item)
        self._total_cache += norm_item.total

    def remove_norm_item(self, norm_item: NormItem) -> None:
        self._total_cache -= norm_item.total
        self.norm_items.remove(norm_item)

    def update_norm_quantity(self, norm_item: NormItem, quantity: int) -> None:
        if norm_item not in self.norm_items:
            return
        self._total_cache -= norm_item.total
        norm_item.quantity = quantity
        self._total_cache += norm_item.total

    @property
    def total(self) -> int:
        return self._total_cache

