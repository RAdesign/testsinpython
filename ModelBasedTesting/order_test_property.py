"""property testing for order class, a difference between property testing and unit testing,
 use recording pytest"""
from hypothesis import given
from hypothesis.strategies import integers

from order import NormItem


# testing property of item
@given(integers(), integers())
def test_norm_item(price: int, quantity: int) -> None:
    norm_item = NormItem("Pepe Bobble-head", price, quantity)
    assert norm_item.total == price * quantity
