Model based testing - How to use hypothesis in it.
Thinking about all the cases.
Model based testing allows for many inputs like property based tests, and also 
different sequences of actions.
Hypothesis chooses which method to call next.
1. write down all possible actions to test
2. validate / add correctness tests to those actions
3. put all those actions and validation tests into a class
4. actions are calls 'rules', validations are called 'invariants'
5. rules and invariants form a 'state machine' that is run by hypothesis
6. adding preconditions with regard of business requirements narrows down tests